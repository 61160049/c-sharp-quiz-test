using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

var app = builder.Build();

// Configure the HTTP request pipeline.

app.MapPost("/FormatNumberRange", (int[] numbers) =>
{
    int count = 0;
    string results = null;
    results += numbers[0];
    for(int i = 1; i < numbers.Length; i++)
    {
        if(i != numbers.Length - 1)
        {
            if ((numbers[i - 1] - numbers[i]) == -1)
            {
                count++;
            }
            else
            {
                if(count > 0)
                {
                    results += "-" + numbers[i-1] + "," + numbers[i];
                    count = 0;
                }
                else
                {
                    results += "," + numbers[i]; 
                }
            }
        }
        else
        {
            if ((numbers[i - 1] - numbers[i]) == -1)
            {
                results += "-" + numbers[i];
            }
            else
            {
                if (count > 0)
                {
                    results += "-" + numbers[i-1] + "," + numbers[i];
                    count = 0;
                }
                else
                {
                    results += "," + numbers[i];
                }
            }
        }
        
    }
    return Results.Json(results);
});

app.MapGet("/Excel", (string column) =>
{
    int result = 0;
    if (column.Length == 1)
    {
        switch (column)
        {
            case "A": result = 1; break;
            case "B": result = 2; break;
            case "C": result = 3; break;
            case "D": result = 4; break;
            case "E": result = 5; break;
            case "F": result = 6; break;
            case "G": result = 7; break;
            case "H": result = 8; break;
            case "I": result = 9; break;
            case "J": result = 10; break;
            case "K": result = 11; break;
            case "L": result = 12; break;
            case "M": result = 13; break;
            case "N": result = 14; break;
            case "O": result = 15; break;
            case "P": result = 16; break;
            case "Q": result = 17; break;
            case "R": result = 18; break;
            case "S": result = 19; break;
            case "T": result = 20; break;
            case "U": result = 21; break;
            case "V": result = 22; break;
            case "W": result = 23; break;
            case "X": result = 24; break;
            case "Y": result = 25; break;
            case "Z": result = 26; break;
        }
    }
    else
    {
        string temp = column;
        string check = "";
        for (int i = 0; i < column.Length; i++)
        {
            if (i != column.Length - 1)
            {
                check = temp.Substring(0, 1);
                switch (check)
                {
                    case "A": result += 1 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "B": result += 2 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "C": result += 3 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "D": result += 4 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "E": result += 5 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "F": result += 6 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "G": result += 7 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "H": result += 8 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "I": result += 9 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "J": result += 10 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "K": result += 11 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "L": result += 12 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "M": result += 13 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "N": result += 14 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "O": result += 15 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "P": result += 16 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "Q": result += 17 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "R": result += 18 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "S": result += 19 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "T": result += 20 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "U": result += 21 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "V": result += 22 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "W": result += 23 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "X": result += 24 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "Y": result += 25 * (int)Math.Pow(26, column.Length - 1 - i); break;
                    case "Z": result += 26 * (int)Math.Pow(26, column.Length - 1 - i); break;
                }
                temp = temp.Substring(1);
            }
            else
            {
                switch (temp)
                {
                    case "A": result += 1; break;
                    case "B": result += 2; break;
                    case "C": result += 3; break;
                    case "D": result += 4; break;
                    case "E": result += 5; break;
                    case "F": result += 6; break;
                    case "G": result += 7; break;
                    case "H": result += 8; break;
                    case "I": result += 9; break;
                    case "J": result += 10; break;
                    case "K": result += 11; break;
                    case "L": result += 12; break;
                    case "M": result += 13; break;
                    case "N": result += 14; break;
                    case "O": result += 15; break;
                    case "P": result += 16; break;
                    case "Q": result += 17; break;
                    case "R": result += 18; break;
                    case "S": result += 19; break;
                    case "T": result += 20; break;
                    case "U": result += 21; break;
                    case "V": result += 22; break;
                    case "W": result += 23; break;
                    case "X": result += 24; break;
                    case "Y": result += 25; break;
                    case "Z": result += 26; break;
                }
            }
        }
    }
    return Results.Json(result);
});

app.MapPost("/Pagination", (PaginationRequest request) =>
{
    int allPage = 0;
    if(request.PageSize > 0)
    {
        if (request.AllRows.Length % request.PageSize != 0)
        {
            allPage = (request.AllRows.Length / request.PageSize) + 1;
        }
        else
        {
            allPage = (request.AllRows.Length / request.PageSize);
       }
    }
    if(request.PageIndex > (allPage - 1))
    {
        request.PageIndex = (allPage - 1);
    }

    PaginationResponse result = new PaginationResponse();
    if (request.PageSize == 0)
    {
        result.From = 0;
        result.To = 0;
        result.Total = request.AllRows.Length;
    }
    else
    {
        int index = request.PageIndex * request.PageSize;
        if (request.PageIndex+1 == allPage)
        {
            result.CurrentPageRows = new int[request.AllRows.Length % request.PageSize];
            for (int i = 0; i < (request.AllRows.Length % request.PageSize); i++)
            {
                result.CurrentPageRows[i] = request.AllRows[index + i];
            }
            result.From = request.AllRows[index]+1;
            result.To = request.AllRows[index]+1;
        }
        else
        {
            result.CurrentPageRows = new int[request.PageSize];
            for (int i = 0; i < request.PageSize; i++)
            {
                result.CurrentPageRows[i] = request.AllRows[index + i];
            }
            result.From = request.AllRows[index]+1;
            result.To = request.AllRows[index + request.PageSize];
        }
        result.Total = request.AllRows.Length;
    }
    return Results.Json(result);
});

app.MapPost("/MatchingBraces", ([FromBody] string text) =>
{
    bool result = true;
    Stack<char> openingBraces = new Stack<char>();
    for (int i = 0; i < text.Length; i++)
    {
        if (text[i] == '(' || text[i] == '{' || text[i] == '[')
        {
            openingBraces.Push(text[i]);
        }
        else if (text[i] == '}')
        {
            if (openingBraces.Count == 0)
            {
                result = false;
            }
            else
            {
                char keep = openingBraces.Pop();
                if (keep != '{')
                {
                    openingBraces.Push(keep);
                }
            }
        }
        else if (text[i] == ')')
        {
            if (openingBraces.Count == 0)
            {
                result = false;
            }
            else
            {
                char keep = openingBraces.Pop();
                if (keep != '(')
                {
                    openingBraces.Push(keep);
                }
            }
        }
        else if (text[i] == ']')
        {
            if (openingBraces.Count == 0)
            {
                result = false;
            }
            else
            {
                char keep = openingBraces.Pop();
                if (keep != '[')
                {
                    openingBraces.Push(keep);
                }
            }
        }
    }
    if (openingBraces.Count > 0)
    {
        result = false;
    }
    return Results.Json(result);
});

app.MapGet("/URI", (string uri) =>
{
    string words = "";
    string words2 = "";
    string words3 = "";
    URIResponse result = new URIResponse();
    //Scheme
    result.Scheme = uri.Split("://")[0];

    //UserInfo
    if (uri.Contains("@"))
    {
        words = uri.Split("@")[0];
        result.UserInfo = words.Split("://")[1];
    }
    
    //Host
    words = uri.Split("://")[1];
    words2 = words.Split("/")[0];
    if (words2.Contains("@"))
    {
        words3 = words2.Split("@")[1];
        if (words3.Contains(":"))
        {
            result.Host = words3.Split(":")[0];
        }
        else
        {
            result.Host = words3;
        }
    }else
    {
        if (words2.Contains(":"))
        {
            result.Host = words2.Split(":")[0];
        }else
        {
            result.Host = words2;
        }
    }

    //Port
    words = uri.Split("://")[1];
    words2 = words.Split("/")[0];
    if (words2.Contains("@"))
    {
        words3 = words2.Split("@")[1];
        if (words3.Contains(":"))
        {
            result.Port = words3.Split(":")[1];
        }
    }
    else
    {
        if (words2.Contains(":"))
        {
            result.Port = words2.Split(":")[1];
        }
    }

    //Path
    words = uri.Split("://")[1];
    string[] splitPath = words.Split("/");
    if (splitPath.Length == 1)
    {
        result.Path = "";
    }
    else
    {
        string value = "";
        if (splitPath.Length > 2)
        {
            value = splitPath[1];
            for (int i = 2; i < splitPath.Length; i++)
            {
                value += "/" + splitPath[i];
            }
        }
        else
        {
            value = splitPath[1];
        }
        if (value.Contains("?"))
        {
            result.Path = value.Split("?")[0];
        }
        else
        {
            if (value.Contains("#"))
            {
                result.Path = value.Split("#")[0];
            }
            else
            {
                result.Path = value;
            }
        }
    }


    //Query
    if (uri.Contains("?"))
    {
        words = uri.Split("?")[1];
        if (words.Contains("#"))
        {
            result.Query = "?" + words.Split("#")[0];
        }
        else
        {
            result.Query = "?" + words;
        }
    }


    //Fragment
    if (uri.Contains("#"))
    {
        words = words.Split("#")[1];
        result.Fragment = "#"+ words;
    }

    return Results.Json(result);
});

app.MapPost("/FindExclusive", (int[][] numbers) =>
{
    int i = 0;
    int[] arr1 = new int[numbers.Length];
    int[] arr2 = new int[numbers.Length];
    foreach (var number in numbers)
    {
        arr1[i] = number[0];
        arr2[i] = number[1];
        i++;
    }
    IEnumerable<int> onlyInFirstSet = arr2.Except(arr1);
    int[] result = onlyInFirstSet.ToArray();
    return Results.Json(result);
});

app.MapPost("/FindCommon", (int[][] numbers) =>
{
    int i = 0;
    int[] arr1 = new int[numbers.Length];
    int[] arr2 = new int[numbers.Length];
    foreach (var number in numbers)
    {
        arr1[i] = number[0];
        arr2[i] = number[1];
        i++;
    }
    IEnumerable<int> onlyInFirstSet = arr1.Intersect(arr2);
    int[] result = onlyInFirstSet.ToArray();
    return Results.Json(result);
});

app.MapPost("/FindAll", (int[][] numbers) =>
{
    int i = 0;
    int[] arr1 = new int[numbers.Length];
    int[] arr2 = new int[numbers.Length];
    foreach (var number in numbers)
    {
        arr1[i] = number[0];
        arr2[i] = number[1];
        i++;
    }
    IEnumerable<int> onlyInFirstSet = arr1.Union(arr2);
    int[] result = onlyInFirstSet.ToArray();
    return Results.Json(result);
});

app.Run();
class PaginationRequest
{
    public int[] AllRows { get; set; } = new int[0];
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
}

class PaginationResponse
{
    public int[] CurrentPageRows { get; set; } = new int[0];
    public int From { get; set; }
    public int To { get; set; }
    public int Total { get; set; }
}

class URIResponse
{
    public string Scheme { get; set; } = string.Empty;
    public string UserInfo { get; set; } = string.Empty;
    public string Host { get; set; } = string.Empty;
    public string Port { get; set; } = string.Empty;
    public string Path { get; set; } = string.Empty;
    public string Query { get; set; } = string.Empty;
    public string Fragment { get; set; } = string.Empty;
}
